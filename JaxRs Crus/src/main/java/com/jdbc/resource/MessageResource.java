package com.jdbc.resource;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.jdbc.entity.Message;
import com.jdbc.service.MessageService;

@Component
@Path("/messages")
public class MessageResource {

	@Autowired
	private MessageService msgService;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Message> getMessageDetails() {
		return msgService.getAllMessages();
	}

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public String getMessageById(@PathParam("id") Integer id) {
     
		msgService.getMessageById(id);
	
		return "Created Successfully";

	}
	
	

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String addMessage(Message message) {
		msgService.addMessage(message);
		return "Created Successfully";

	}
	


	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateMessage(Message message) {
		msgService.updateMessage(message);
		return Response.ok(message).build();

	}

	@DELETE
	@Path("/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	public String deleteMessage(@PathParam("id") int id) {
		msgService.deleteMessage(id);
		return "Deleted Successfully";

	}
	
	
	

	

}
