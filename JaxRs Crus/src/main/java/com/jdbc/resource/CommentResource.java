package com.jdbc.resource;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.jdbc.entity.Comment;
import com.jdbc.service.CommentService;

@Component
@Path("/comments")
public class CommentResource {
	
	@Autowired
	private CommentService cmtService;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Comment> getCommentDetails() {
		return cmtService.getAllComments();
	}
	
	
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCommentById(@PathParam("id") Integer id) {
		Comment cmt = cmtService.getCommentById(id);
		return Response.ok(cmt).build();

	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String addComment(Comment comment) {
		cmtService.addComment(comment);
		return "Created Successfully";

	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateComment(Comment comment) {
		cmtService.updateComment(comment);
		return Response.ok(comment).build();

	}

	@DELETE
	@Path("/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	public String deleteComment(@PathParam("id") int id) {
		cmtService.deleteComment(id);
		return "Deleted Successfully";

	}
	


}
