package com.jdbc.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Comment")
public class Comment {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "CommentId")
	private int commentId;
	@Column(name = "Review")
	private String review;

	
	public Comment() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Comment(int commentId, String review) {
		super();
		this.commentId = commentId;
		this.review = review;

	}

	public int getCommentId() {
		return commentId;
	}

	public void setCommentId(int commentId) {
		this.commentId = commentId;
	}

	public String getReview() {
		return review;
	}


	public void setReview(String review) {
		this.review = review;
	}

	@Override
	public String toString() {
		return "Comment [commentId=" + commentId + ", review=" + review + "]";
	}

	

}
