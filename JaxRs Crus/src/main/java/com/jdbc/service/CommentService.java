package com.jdbc.service;

import java.util.List;

import com.jdbc.entity.Comment;

public interface CommentService {
	
	public List<Comment> getAllComments();
	
	public Comment getCommentById(int commentId);
	
	public void addComment(Comment comment) ;
	public void updateComment(Comment comment);
	public void deleteComment(int id);

}
