package com.jdbc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.jdbc.dao.MessageDao;
import com.jdbc.entity.Message;

@Service
public class MessageServiceImpl implements MessageService { 
	
	@Autowired
	private MessageDao messageDao;

	@Override
	public List<Message> getAllMessages() {
		return messageDao.getAllMessages();
	}

	@Override
	public Message getMessageById(int id) {
		Message msg = messageDao.getMessageById(id);
		return msg;
	}

	@Override
	public void addMessage(Message message) {
		messageDao.addMessage(message);
		
	}

	@Override
	public void updateMessage(Message message) {
		messageDao.updateMessage(message);
		
	}

	@Override
	public void deleteMessage(int id) {
		messageDao.deleteMessage(id);
		
	}
		

}
