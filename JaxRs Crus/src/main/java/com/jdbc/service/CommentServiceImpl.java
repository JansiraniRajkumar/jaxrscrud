package com.jdbc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jdbc.dao.CommentDao;
import com.jdbc.entity.Comment;

@Service
public class CommentServiceImpl implements CommentService{
	
	@Autowired
	private CommentDao cmtDao;

	@Override
	public List<Comment> getAllComments() {
		return cmtDao.getAllComments();
	}

	@Override
	public Comment getCommentById(int commentId) {
		Comment cmt = cmtDao.getCommentById(commentId);
		return cmt;
	}

	@Override
	public void addComment(Comment comment) {
		cmtDao.addComment(comment);
		
	}

	@Override
	public void updateComment(Comment comment) {
		cmtDao.updateComment(comment);
		
	}

	@Override
	public void deleteComment(int id) {
		cmtDao.deleteComment(id);
		
	}
	
	

}
