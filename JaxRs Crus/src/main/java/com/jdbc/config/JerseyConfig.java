package com.jdbc.config;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

import com.jdbc.resource.CommentResource;
import com.jdbc.resource.MessageResource;


@Component
@ApplicationPath("/webapi")
public class JerseyConfig extends ResourceConfig {

	public JerseyConfig() {
		register(MessageResource.class);
		register(CommentResource.class);
	}
	
	
}
