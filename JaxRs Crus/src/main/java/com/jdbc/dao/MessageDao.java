package com.jdbc.dao;

import java.util.List;
import com.jdbc.entity.Message;


public interface MessageDao {
	
	 List<Message> getAllMessages();
	 Message getMessageById(int id);
	 void addMessage(Message message);
	 void updateMessage(Message message);
	 void deleteMessage(int id);

}
