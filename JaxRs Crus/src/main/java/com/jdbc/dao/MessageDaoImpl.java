package com.jdbc.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;
import com.jdbc.entity.Message;

@Transactional
@Repository
public class MessageDaoImpl implements MessageDao {

	@PersistenceContext
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Override
	public List<Message> getAllMessages() {
		String hql = "FROM Message as msg ORDER BY msg.id";
		return (List<Message>) entityManager.createQuery(hql).getResultList();
	}

	@Override
	public Message getMessageById(int id) {
		return entityManager.find(Message.class, id);

	}

	@Override
	public void addMessage(Message message) {
		entityManager.persist(message);

	}

	@Override
	public void updateMessage(Message message) {
		Message msg = getMessageById(message.getId());
		msg.setName(message.getName());
		msg.setAuthor(message.getAuthor());
		entityManager.flush();

	}

	@Override
	public void deleteMessage(int id) {
		entityManager.remove(getMessageById(id));
		
	}

}
