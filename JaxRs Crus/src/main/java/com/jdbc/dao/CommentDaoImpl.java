package com.jdbc.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.jdbc.entity.Comment;


@Repository
@Transactional
public class CommentDaoImpl implements CommentDao{
	

	@PersistenceContext
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Override
	public List<Comment> getAllComments() {
		String hql = "FROM Comment";
		return (List<Comment>) entityManager.createQuery(hql).getResultList();
	}
	
	@Override
	public Comment getCommentById(int commentId) {
		return entityManager.find(Comment.class, commentId);

	}
	

	@Override
	public void addComment(Comment comment) {
		entityManager.persist(comment);

	}

	@Override
	public void updateComment(Comment comment) {
		Comment cmt = getCommentById(comment.getCommentId());
		cmt.setCommentId(cmt.getCommentId());
		cmt.setReview(comment.getReview());
		entityManager.flush();

	}

	@Override
	public void deleteComment(int id) {
		entityManager.remove(getCommentById(id));
		
	}

}
